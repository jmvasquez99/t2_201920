package view;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.Viaje;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar datos del primer trimestre 2018");
			System.out.println("2. Buscar cluster ascendente mas grande");
			System.out.println("3. Reportar ultimos viajes de alguna hora");
			System.out.println("0. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}	
		
		public void printQueue(Queue<Viaje> queue) {
			while(queue.size()>0) {
				System.out.println(queue.dequeue());
			}
		}
		
		public void printStack(Stack<Viaje> stack) {
			while(stack.size()>0) {
				System.out.println(stack.pop());
			}
		}
}
