package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargar datos del primer semestre: ");
				    modelo = new MVCModelo(); 
				    modelo.cargarDatos();
					break;
					
				case 11:
					System.out.println("--------- \nCargar datos de prueba del primer semestre: ");
				    modelo = new MVCModelo(); 
				    modelo.clusterAscendenteMasGrande();
					break;

				case 2:
					System.out.println("--------- \nBuscar el cluster ascendente mas grande: ");
				    modelo = new MVCModelo(); 
				    view.printQueue(modelo.clusterAscendenteMasGrande());
					break;
					
				case 3:
					System.out.println("--------- \nReportar ultimos viajes de alguna hora: ");
				    modelo = new MVCModelo();
				    System.out.println("Ingrese la cantidad de viajes que quiere ver: ");
				    respuesta = lector.nextLine();
				    int i = Integer.parseInt(respuesta);
				    System.out.println("Ingrese la hora de los viajes que quiere ver: ");
				    respuesta = lector.nextLine();
				    int j = Integer.parseInt(respuesta);
				    view.printStack(modelo.ultimosNViajesHoraH(i, j));
					break;
					
				case 0: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
			
			System.out.println("Presione [ENTER] para continuar");
			lector.nextLine();
		}
		
	}	
}
