package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.Queue;
import model.data_structures.Stack;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Pila
	 */
	private Stack<Viaje> pila;

	/**
	 * Cola  
	 */
	private Queue<Viaje> cola;

	/**
	 * Constructor del modelo del mundo creando una cola y una pila
	 */
	public MVCModelo()
	{
		pila = new Stack<Viaje>();
		cola = new Queue<Viaje>();
	}

	/**
	 * Carga los datos 
	 */
	public void cargarDatos() {

		pila = new Stack<Viaje>();
		cola = new Queue<Viaje>();

		try {

			System.out.println("---------- Cargando Datos 2018Q1 ----------");

			CSVReader reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));

			String [] nextLine;
			reader.readNext();
			while ((nextLine = reader.readNext()) != null) {
				int nl0 	= Integer.parseInt(nextLine[0]);
				int nl1	= Integer.parseInt(nextLine[1]);
				int nl2 	= Integer.parseInt(nextLine[2]);
				double nl3 = Double.parseDouble(nextLine[3]);
				double nl4 = Double.parseDouble(nextLine[4]);
				double nl5 = Double.parseDouble(nextLine[5]);
				double nl6 = Double.parseDouble(nextLine[6]);

				Viaje temp = new Viaje(nl0, nl1, nl2, nl3, nl4, nl5, nl6);
				pila.push(temp);
				cola.enqueue(temp);
			}
			System.out.println("---------- Datos 2018Q1 Cargados ----------");
			
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error en la carga: Error con el archivo");
			return;
		} catch (IOException e) {
			System.out.println("Error en la carga: Error con el reader");
		}
		
		System.out.println("Numero de viajes cargados en la cola: " + cola.size());
		System.out.println("Numero de viajes cargados en la pila: " + pila.size());
		System.out.println("Primer viaje en cargar: " + cola.darPrimero());
		System.out.println("Ultimo viaje en cargar: " + cola.darUltimo());
	}
	
	/**
	 * Carga los datos 
	 */
	public void cargarDatosPrueba() {

		pila = new Stack<Viaje>();
		cola = new Queue<Viaje>();

		try {

			System.out.println("---------- Cargando Datos Prueba 2018Q1 ----------");

			CSVReader reader = new CSVReader(new FileReader("./data/datosP1"));

			String [] nextLine;
			reader.readNext();
			while ((nextLine = reader.readNext()) != null) {
				int nl0 	= Integer.parseInt(nextLine[0]);
				int nl1	= Integer.parseInt(nextLine[1]);
				int nl2 	= Integer.parseInt(nextLine[2]);
				double nl3 = Double.parseDouble(nextLine[3]);
				double nl4 = Double.parseDouble(nextLine[4]);
				double nl5 = Double.parseDouble(nextLine[5]);
				double nl6 = Double.parseDouble(nextLine[6]);

				Viaje temp = new Viaje(nl0, nl1, nl2, nl3, nl4, nl5, nl6);
				pila.push(temp);
				cola.enqueue(temp);
			}
			System.out.println("---------- Datos Prueba 2018Q1 Cargados ----------");
			
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error en la carga: Error con el archivo");
			return;
		} catch (IOException e) {
			System.out.println("Error en la carga: Error con el reader");
		}
		
		System.out.println("Numero de viajes cargados en la cola: " + cola.size());
		System.out.println("Numero de viajes cargados en la pila: " + pila.size());
		System.out.println("Primer viaje en cargar: " + cola.darPrimero());
		System.out.println("Ultimo viaje en cargar: " + cola.darUltimo());
	}
	
	/**
	 * Este metodo retorna una cola/queue con el cluster de viajes ascendente mas grande
	 * 
	 * <post> la cola del modelo queda vacia, por lo cual toca volver a cargar los datos.
	 * @return
	 */
	public Queue<Viaje> clusterAscendenteMasGrande() {
		Queue<Viaje> mayor = new Queue<Viaje>();
		Queue<Viaje> temp  = new Queue<Viaje>();
		
		while(cola.size()>0) {
			Viaje c = cola.dequeue(); 
			
			if(temp.darUltimo()==null || temp.darUltimo().darHOD() > c.darHOD()) {
				temp = new Queue<Viaje>();	
			}
			
			temp.enqueue(c);
			
			if(temp.size()>mayor.size()) {
				mayor = temp;
			}
		}
		
		return mayor;
	}
	
	/**
	 * Este metodo retorna una pila stack con los ultimos n viajes de la hora h.
	 * @param n - int numero de viajes
	 * @param h - int hora
	 * 
	 * <post> la pila del modelo queda vacia o incompleta, por lo cual toca volver a cargar los datos.
	 * @return
	 */
	public Stack<Viaje> ultimosNViajesHoraH(int n, int h) {
		Stack<Viaje> ultimos = new Stack<Viaje>();
		
		while(n>0 && pila.size()>0) {
			Viaje v = pila.pop();
			if(v.darHOD() == h) {
				ultimos.push(v);
				n--;
			}
		}
		
		return ultimos;
	}

	public int darColaTamano()
	{
		return cola.size();
	}
	
	public int darPilaTamano()
	{
		return pila.size();
	}
}

	