package model.logic;

public class Viaje {

	private int sourceid;
	private int dstid;
	private int hod;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double pritgeometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;
	
	public Viaje(int sourceid, int dstid, int hod, double mean, double stdt, double prit, double geom) {
		this.sourceid = sourceid;
		this.dstid = dstid;
		this.hod = hod;
		mean_travel_time = mean;
		standard_deviation_travel_time = stdt;
		pritgeometric_mean_travel_time = prit;
		geometric_standard_deviation_travel_time = geom;
	}
	
	//
	// Metodos dar
	
	public int darSourceId() {
		return sourceid;
	}
	
	public int darDstId() {
		return dstid;
	}
	
	public int darHOD() {
		return hod;
	}
	
	public double darMeanTT() {
		return mean_travel_time;
	}
	
	public double darStdDeviationTT() {
		return standard_deviation_travel_time;
	}
	
	public double darPritgeometricTT() {
		return pritgeometric_mean_travel_time;
	}
	
	public double darGeometricSDTT() {
		return geometric_standard_deviation_travel_time;
	}
	
	//
	// TO STRING
	
	/**
	 * Da un viaje en String para que se pueda imprimir al usuario.
	 */
	@Override
	public String toString() {
		String msg = "[";
		msg+= "Origen: " +sourceid+"->Destino: "+dstid+" Hora:"+hod;
		double tiempo = mean_travel_time/60;
		msg+= " Tiempo promedio: "+ ((int)tiempo) + "min ";
		tiempo = (tiempo - ((int)tiempo))*60;
		msg+= ((int) tiempo) + "s ";
		msg+= "Desviacion Std: " + standard_deviation_travel_time + " ]";
		return msg;
	}
}
