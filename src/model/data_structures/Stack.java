package model.data_structures;

/**
 * Pila
 * @author juandiego & juanmanuel
 *
 * @param <T>
 */

public class Stack<T>{
	
	private int cantidadElementos;
	
	private Node<T> primero;
	
	public Stack() {
		cantidadElementos = 0;
		primero = null;
	}
	
	public void push(T elem) {
		Node<T> temp = primero;
		primero = new Node<T>(elem);
		primero.cambiarSiguiente(temp);
		cantidadElementos++;
	}
	
	public T pop() {
		if(cantidadElementos>0) {
			cantidadElementos--;
			T temp = primero.darElemento();
			primero = primero.darSiguiente();
			return temp;
		}
		return null;
	}

	public int size() {
		return cantidadElementos;
	}
	
	public T darPrimero() {
		if(primero!=null)
			return primero.darElemento();
		return null;
	}
	
}
