package model.data_structures;

/**
 * Cola
 * @author juandiego & juanmanuel
 *
 * @param <T>
 */
public class Queue<T> {
	
	private Node<T> primero;
	
	private Node<T> ultimo;
	
	private int cantidadElementos;
	
	public Queue() {
		primero = null;
		ultimo = null;
		cantidadElementos= 0;
	}
	
	public void enqueue(T elem) {
		if(primero==null) {
			primero = new Node<T>(elem);
			ultimo = primero;
		}else {
			ultimo.cambiarSiguiente(new Node<T>(elem));
			ultimo = ultimo.darSiguiente();
		}
		cantidadElementos++;
	}
	
	public T dequeue() {
		if(cantidadElementos==0) 
			return null;
		
		T temp = primero.darElemento();
		primero = primero.darSiguiente();
		cantidadElementos--;
		return temp;
	}
	
	public int size() {
		return cantidadElementos;
	}
	
	public T darUltimo() {
		if(ultimo!=null)
			return ultimo.darElemento();
		return null;
	}
	
	public T darPrimero() {
		if(primero!=null)
			return primero.darElemento();
		return null;
	}
}
