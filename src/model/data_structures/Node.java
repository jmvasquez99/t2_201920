package model.data_structures;

/**
 * Esta es una clase auxiliar para los elementos de la lista.
 * 
 * @author juandiego
 *
 * @param <T>
 */
public class Node <T> {
	
	
	/**
	 * Elemento contenido por el nodo.
	 */
	private T elemento;
	
	/**
	 * Siguiente elemento.
	 */
	private Node<T> siguiente;
	
	
	/**
	 * Constructor basico de un nodo
	 */
	public Node(T nuevo) {
		elemento = nuevo;
	}
	
	
	//_____________________________________________________________________________
	// METODOS DAR
	//_____________________________________________________________________________
	
	/**
	 * Da el elemento
	 * @return T elemento
	 */
	public T darElemento() {
		return elemento;
	}

	/**
	 * Da el siguiente
	 * @return T siguiente
	 */
	public Node<T> darSiguiente() {
		return siguiente;
	}
	
	
	//_____________________________________________________________________________
	// METODOS CAMBIAR
	//_____________________________________________________________________________
	
	/**
	 * Cambia el siguiente a un elemento nodo que entra por parametro
	 * 
	 * @param nuevo nuevo elemento
	 */
	public void cambiarSiguiente(Node<T> nuevo) {
		siguiente = nuevo;
	}
	
	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenar· en el nodo.
	 */
	public void cambiarElemento(T elemento)
	{
		this.elemento = elemento;
	}
}
