package test.logic;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.Viaje;

public class TestMVCModelo {

	private Queue<Integer>  cola;
	private Stack<Integer>  pila;

	@Before
	public void setUp1() {
		cola= new Queue<Integer>();
		pila= new Stack<Integer>();
	}

	public void setUpQueue() {
		cola.enqueue(3); 
		cola.enqueue(4); 
		cola.enqueue(5); 
		cola.enqueue(11); 
		cola.enqueue(11); 
		cola.enqueue(12); 
		cola.enqueue(13); 
		cola.enqueue(24); 
		cola.enqueue(3); 
		cola.enqueue(4); 
		cola.enqueue(5); 
		cola.enqueue(6); 
		cola.enqueue(2); 
		cola.enqueue(3); 
		cola.enqueue(14); 
		cola.enqueue(1); 
		cola.enqueue(2); 
		cola.enqueue(3); 
		cola.enqueue(1); 
	}
	
	public void setUpStack() {
		pila.push(0); 
		pila.push(1); 
		pila.push(2); 
		pila.push(3); 
		pila.push(4); 
		pila.push(5); 
		pila.push(6); 
		pila.push(7); 
		pila.push(8); 
		pila.push(9); 
		pila.push(10); 
		pila.push(11); 
		pila.push(12); 
		pila.push(13); 
		pila.push(14); 
		pila.push(15); 
	}
	
	@Test
	public void testQueue() {
		Stack<Integer> prueba = ultimosNintsMayoraH(3, 1);
		assertEquals(prueba.pop(),null);
		
		setUpStack();
		prueba = ultimosNintsMayoraH(3, 10);
		
		assertEquals(prueba.pop().intValue(),13);
		assertEquals(prueba.pop().intValue(),14);
		assertEquals(prueba.pop().intValue(),15);

		assertEquals(pila.size(),13);
		assertEquals(prueba.size(),0);
	}
	
	@Test
	public void testStack() {
		Queue<Integer> prueba = clusterAscendenteMasGrande();
		assertEquals(prueba.dequeue(),null);
		
		setUpQueue();
		prueba = clusterAscendenteMasGrande();
		
		assertEquals(prueba.dequeue().intValue(),3);
		assertEquals(prueba.dequeue().intValue(),4);
		assertEquals(prueba.dequeue().intValue(),5);
		assertEquals(prueba.dequeue().intValue(),11);
		assertEquals(prueba.dequeue().intValue(),11);
		assertEquals(prueba.dequeue().intValue(),12);
		assertEquals(prueba.dequeue().intValue(),13);
		assertEquals(prueba.dequeue().intValue(),24);
		assertEquals(prueba.size(),0);
		assertEquals(cola.size(),0);
	}
	
	//Metodos del MVC Modelo a probar en esta misma clase
	
	public Queue<Integer> clusterAscendenteMasGrande() {
		Queue<Integer> mayor = new Queue<Integer>();
		Queue<Integer> temp  = new Queue<Integer>();
		
		while(cola.size()>0) {
			int c = cola.dequeue(); 
			
			if(temp.darUltimo()==null || temp.darUltimo() > c) {
				temp = new Queue<Integer>();	
			}
			
			temp.enqueue(c);
			
			if(temp.size()>mayor.size()) {
				mayor = temp;
			}
		}
		return mayor;
	}
	
	/**
	 * Este metodo hace lo mismo que ultimos n viajes de una hora pero con enteros
	 * Esto da los ultimos n numeros mayores o iguales a h
	 * @param n
	 * @param h
	 * @return
	 */
	public Stack<Integer> ultimosNintsMayoraH(int n, int h) {
		Stack<Integer> ultimos = new Stack<Integer>();
		
		while(n>0 && pila.size()>0) {
			int v = pila.pop().intValue();
			if(v >= h) {
				ultimos.push(v);
				n--;
			}
		}
		
		return ultimos;
	}
}
