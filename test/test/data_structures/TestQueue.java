package test.data_structures;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;


public class TestQueue {
	
	private Queue<String>  datos;
	
	@Before
	public void setUp1() {
		datos= new Queue<String>();
	}

	@Test
	public void test() {
		
		for(int i = 1; i <= 5; i++) {
			datos.enqueue(""+i);
		}
		
		assertEquals(datos.size(), 5);
		
		for(int i = 1; i <= 5; i++) {
			datos.dequeue();
		}
		
		assertEquals(datos.size(), 0);
		
		datos.enqueue("Listo");
		assertEquals(datos.size(), 1);
		
	}
}