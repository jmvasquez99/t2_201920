package test.data_structures;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Stack;


public class TestStack {
	
	private Stack<String>  lista;
	
	@Before
	public void setUp1() {
		lista= new Stack<String>();
	}

	@Test
	public void test() {
		
		for(int i = 1; i <= 5; i++) {
			lista.push(""+i);
		}
		
		assertEquals(lista.size(), 5);
		
		for(int i  = 5; i >= 1; i--) {
			String t = lista.pop();
			System.out.println(t);
		}
		
		assertEquals(lista.size(), 0);
		
		lista.push("Listo");
		assertEquals(lista.size(), 1);
		
	}
}
